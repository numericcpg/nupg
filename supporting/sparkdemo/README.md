# NumericcOrg

Publishing area for Numericc.Org artefacts

csv dir contains csv files for commit and issues

sql dir contains sql files to create tables

Steps to build from scratch:

* Create tables using SQL from sql dir
* Use Postgres COPY command to import CSV files into tables

dump/pg contains pg_dump sql file approx 50MB 

you will need to create a Postgres database and then use pg_restore to create tables, import all the data into them 

If you use the pg_dump file to do a pg_restore you dont have to do anything else and don't need the csv or sql files. 

This is all very early please log issues if this doesn't work as claimed

License: 

All data released under Creative Commons Attribution-NonCommercial-ShareAlike (CC BY-NC-SA)

The data restrictions will be loosened up after more data cleaning. This is very preliminary and a proof of concept

All code released under Apache 2.0 license

Slack channel:   

https://join.slack.com/t/numericcorg/shared_invite/enQtNzkzNjc0MzY4NDY3LTlhMmRhMDlmYzJmYjcwMzAzODFjY2NlNmFkOGU4OTcyZGYwNGM1ZjgxMTk0NzE0YTMzZDFhN2I1NGFkNGYxNTg
