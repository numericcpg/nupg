import glob
import subprocess


csvsql_dir ='/Users/nitin/anaconda/envs/nupg/bin/'


def make_sample_file(file_name, num_lines):
    with open(file_name) as f:
        lines = f.readlines()
        # assuming first line of csv file is a header of col names.
        # drop first line keep next num_lines
        out_lines = lines[:num_lines+1]
        sample_file_name = file_name + '_sample_' + str(num_lines)
        write_file_lines(out_lines,sample_file_name)
    return sample_file_name


def write_file_lines(lines, file_name):
    with open(file_name,'w') as f:
        f.writelines(lines)
    return


def write_file(content, file_name):
    with open(file_name,'w') as f:
        f.write(str(content,'utf-8'))
    return


def make_ddl_for_csv_file(csv_file,sql_file, use_sample=True, sample_size=5000):
    global csvsql_dir
    print("inside make_ddl_for_csv \n {} --- {}".format(csv_file, sql_file))
    if use_sample:
        src_file_name = make_sample_file(csv_file,sample_size)
    else:
        src_file_name = csv_file

    ddl = subprocess.check_output([csvsql_dir+'csvsql', '-i', 'postgresql', src_file_name])
    print("ddl: {}".format(str(ddl)))
    write_file(ddl,sql_file)
    return


def make_ddl(input_file):

    return


if __name__ == '__main__':
    # these should go in a conf file or on cmd line in general
    csv_glob='/Users/nitin/nupg/csv/SPARK_*.csv'
    new_ext='sql'
    csv_files = glob.glob(csv_glob)
    # replace csv with sql
    sql_files = ['.'.join([file_name.split('.')[:-1][0], new_ext]) for file_name in csv_files]
    print(sql_files[0])
    file_pairs = zip(csv_files, sql_files)  # tuples with src and dest filenames
    #print(list(file_pairs)[0])
    print("{} --- {}".format(csv_glob, new_ext))
    print(".....")
    [make_ddl_for_csv_file(x,y) for (x,y) in list(file_pairs)]

    print("###")