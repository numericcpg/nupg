const { Client } = require('pg').Client;
const client = new Client();
client.connect();
client
    .query('SELECT $1::text as name', ['nitin'])
    .then(result => console.log(result))
    .catch(e => console.error(e.stack))
    .then(() => client.end());