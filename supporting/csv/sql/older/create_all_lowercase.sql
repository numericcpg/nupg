SET search_path = spark;
CREATE TABLE spark_all_issue_records (
	key VARCHAR , 
	jira_id DECIMAL , 
	status_category VARCHAR , 
	creator VARCHAR, 
	priority VARCHAR , 
	status VARCHAR , 
	assignee VARCHAR, 
	issuetype VARCHAR , 
	reporter VARCHAR, 
	resolution VARCHAR, 
	project VARCHAR , 
	updated TIMESTAMP WITHOUT TIME ZONE, 
	created TIMESTAMP WITHOUT TIME ZONE, 
	resolved TIMESTAMP WITHOUT TIME ZONE, 
	components VARCHAR, 
	severity DECIMAL 
);
CREATE TABLE spark_commit_records (
	cid VARCHAR , 
	message VARCHAR , 
	author_name VARCHAR, 
	author_email VARCHAR, 
	committer_name VARCHAR, 
	committer_email VARCHAR, 
	time DECIMAL, 
	time_offset DECIMAL, 
	tree_id VARCHAR, 
	message_encoding VARCHAR
);
CREATE TABLE spark_jira_key_sev (
	key VARCHAR , 
	severity DECIMAL 
);
CREATE TABLE spark_sha_files (
	sha VARCHAR , 
	filename VARCHAR 
);
CREATE TABLE spark_sha_jira_key_module_file (
	sha VARCHAR , 
	key VARCHAR , 
	module VARCHAR , 
	filename VARCHAR 
);
CREATE TABLE spark_sha_jira_keys (
	sha VARCHAR , 
	key VARCHAR 
);
