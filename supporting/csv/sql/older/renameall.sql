set search_path=spark;
ALTER TABLE "SPARK_all_issue_records" RENAME TO spark_all_issue_records;
ALTER TABLE "SPARK_commit_records" RENAME TO spark_commit_records;
ALTER TABLE "SPARK_jira_key_sev" RENAME TO spark_jira_key_sev;
ALTER TABLE "SPARK_sha_files" RENAME TO spark_sha_files;
ALTER TABLE "SPARK_sha_jira_key_module_file" RENAME TO spark_sha_jira_key_module_file;
ALTER TABLE "SPARK_sha_jira_keys" RENAME TO spark_sha_jira_keys;