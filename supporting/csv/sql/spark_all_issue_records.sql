CREATE TABLE "SPARK_all_issue_records" (
	key VARCHAR , 
	jira_id DECIMAL , 
	status_category VARCHAR , 
	creator VARCHAR, 
	priority VARCHAR , 
	status VARCHAR , 
	assignee VARCHAR, 
	issuetype VARCHAR , 
	reporter VARCHAR, 
	resolution VARCHAR, 
	project VARCHAR , 
	updated TIMESTAMP WITHOUT TIME ZONE, 
	created TIMESTAMP WITHOUT TIME ZONE, 
	resolved TIMESTAMP WITHOUT TIME ZONE, 
	components VARCHAR, 
	severity DECIMAL 
);
