factsupportingcode.sql
----------------------


#### REPORTS

#commits_issues_by_project
select project, count(distinct sha) as commits, count(distinct key) 
as issues from spark_core_facts group by project

#file_changes
select filename, count(sha) as changes from spark_issue_facts
group by filename order by changes desc limit 100


select author_name, count(distinct cid) as commits from spark_issue_commit_LOJ group by author_name order by commits desc

#quality_ratio_spark
select 
	(select count(*) from `spark_issue_lifecycle_facts` 
	where isOpen=False and project='Spark')
	/ # division sign
	(select count(*) from `spark_issue_lifecycle_facts` 
	where isOpen=True and issueType='Bug'and project='Spark') as QR

#yr_mo_day_etc_sev
select yr, qtr ,mo, day, project, issuetype, status, severity, action, key 
from `spark_issue_lifecycle_facts` where yr > 2000 
order by 
yr desc, qtr, mo, day, action, project, issuetype, status, severity desc


#spark_bug_dates
select key, jira_id, status, components, issuetype, created, 
updated, resolved, priority, severity 
from 
bench00.spark_all_issue_records
where issuetype = 'Bug'

#spark_nonbug_dates
select key, jira_id, status, components, issuetype, created, updated, resolved, priority, severity from bench00.spark_all_issue_records
where issuetype != 'Bug'

#sumsev_yr_day_mo
select yr, qtr ,mo, day, project, issuetype, status ,sum(severity)as sumsev from `spark_issue_lifecycle_facts` where yr > 2000 
group by yr,qtr,mo,day,project,issuetype,status order by yr desc, qtr, mo, day

#temp_severity_counts
#select severity, count(severity) as num, sum(severity) as tot from `spark_five_core_facts` where status = 'Reopened' group by severity order by tot desc
select severity, count(severity) as num, sum(severity) as tot from `spark_five_core_facts` group by severity order by tot desc
#select count(*) from`spark_all_commit_records

select author_name, count(distinct cid) as commits from `bench00.spark_issue_commit_LOJ` group by author_name order by commits desc


#spark_contrib_commits
select project, count(distinct sha) as commits, 
count(distinct key) as issues 
from bench00.fiveplus 
group by project

#spark_issues_count_medium
select status, count(status) as cnt from 
`bench00.spark_all_issue_records` 
where severity = 3 
group by status order by cnt desc

#spark_issues_group_count
select issuetype, status, severity, count(issuetype) as cnt 
from `bench00.spark_all_issue_records` 
where severity=3 and 
(status='Open' or status='Closed' or (status='Resolved' and resolution='Fixed'))
group by issuetype, status, severity order by issuetype, status, severity desc, cnt desc


#spark_issues_type_count
select issuetype, count(issuetype) as cnt from `bench00.spark_all_issue_records` group by issuetype
order by cnt desc


#-------------
