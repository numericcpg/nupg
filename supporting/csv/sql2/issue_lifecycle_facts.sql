-- pipeline 2
-- spark_all_issue_records
-- spark_commit_records
-- spark_jira_key_sev
-- spark_sha_files
-- spark_sha_jira_key_module_file
-- spark_sha_jira_keys

-- create all fact tables

-- prep

create table spark_datedim as (SELECT day
FROM UNNEST(
    GENERATE_DATE_ARRAY(DATE('2010-01-01'), DATE('2019-12-31'), INTERVAL 1 DAY)
) AS day)


## ISSUE_LIFECYCLE_FACTS

# issue_lifecycle_facts
# needs: all_issue_records

create table spark.spark_issue_lifecycle_facts as
select 
project,
extract(YEAR from created) as yr,
extract(QUARTER from created) as qtr,
extract(MONTH from created) as mo, 
extract(DAY from created) as day,
'created' as action, issuetype, status, key, severity
from spark.spark_all_issue_records where created is not null

UNION ALL

select 
project,
extract(YEAR from updated) as yr,
extract(QUARTER from updated) as qtr,
extract(MONTH from updated) as mo, 
extract(DAY from updated) as day,
'updated' as action, issuetype, status, key, severity
from spark.spark_all_issue_records where updated is not null

UNION ALL

select 
project,
extract(YEAR from resolved) as yr,
extract(QUARTER from resolved) as qtr,
extract(MONTH from resolved) as mo, 
extract(DAY from resolved) as day,
'resolved' as action, issuetype, status, key, severity
from spark.spark_all_issue_records where resolved is not null;


ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN vpts integer;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN vdel integer;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN workdone integer;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN bugorfeature varchar(10);

ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN isbug boolean;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN isopen boolean;

ALTER TABLE spark_issue_lifecycle_facts
ALTER COLUMN yr type integer,
ALTER COLUMN qtr type integer,
ALTER COLUMN day type integer,
ALTER COLUMN mo type integer,
ALTER COLUMN action type varchar(10),
ALTER COLUMN severity type integer;

-- #UpdateIsBugIsOpen
update spark.spark_issue_lifecycle_facts
	set 
	isbug =  (case when issuetype='Bug' then 1::boolean else 0::boolean end), 
	isopen = (case when status='Open' then 1::boolean else 0::boolean end) ;

-- #value_points
update spark.spark_issue_lifecycle_facts
	set vpts = severity * (case when isbug=1::boolean then -1 else 1 end) ;

-- #work_done
update spark.spark_issue_lifecycle_facts
	set workdone = (case when (status='Closed' or status='Resolved') then abs(vpts) else 0 end) ;
	
-- #value_delivered
update spark.spark_issue_lifecycle_facts 
set vdel = workdone + (case when (isopen=1::boolean and isbug=1::boolean) then vpts else 0 end) ;

-- #FB
update spark.spark_issue_lifecycle_facts
set bugorfeature = (case when issuetype='Bug' then 'Bug' else 'Feature' end) ;






