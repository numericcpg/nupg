
## CORE FACTS

#spark_core_facts


-- =========

-- test: select (REGEXP_MATCHES('SPARK-1234','(SPARK-\d{1,10})'))[1];


create table spark_commit_records_jkey as 
select *, (select (REGEXP_MATCHES(message, '(SPARK-\d{1,10})'))[1] as jkey DEFAULT 'NOKEY') from spark_commit_records;


create table spark_core_facts as (
	select * from 
	spark_commit_records_jkey a 
	full outer join 
	spark_all_issue_records b 
	on a.jkey=b.key
	);

-- =========
