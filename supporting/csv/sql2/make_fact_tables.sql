-- create all fact tables


-- prep

#all_commits_plus (ver 2)
create table all_commits_plus as 
(
	select *,
	EXTRACT( YEAR FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_yr,
	EXTRACT( QUARTER FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_qtr,
	EXTRACT( MONTH FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_mon,
	EXTRACT( WEEK FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_wk,
	EXTRACT( DAY FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_day
	from `all_commit_records`
)


## CORE FACTS

#cassandra_core_facts
create table bench5.cassandra_core_facts as (
select *, REGEXP_EXTRACT(message, "(CASSANDRA-\\d{1,10})") as jkey from bench00.cassandra_commit_records a
full outer join 
bench00.cassandra_all_issue_records b
on 
(REGEXP_EXTRACT(a.message, "(CASSANDRA-\\d{1,10})")=b.key)
)

## ISSUE_LIFECYCLE_FACTS

#issue_lifecycle_facts

create table bench5.issue_lifecycle_facts as
(select 
project,
extract(YEAR from created) as yr,
extract(QUARTER from created) as qtr,
extract(MONTH from created) as mo, 
extract(DAY from cr  eated) as day,
'created' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where created is not null)

UNION ALL

(select 
project,
extract(YEAR from updated) as yr,
extract(QUARTER from updated) as qtr,
extract(MONTH from updated) as mo, 
extract(DAY from updated) as day,
'updated' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where updated is not null)

UNION ALL

(select 
project,
extract(YEAR from resolved) as yr,
extract(QUARTER from resolved) as qtr,
extract(MONTH from resolved) as mo, 
extract(DAY from resolved) as day,
'resolved' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where resolved is not null)


## add JKEY to commits with NOKEY 

#create_commit_jk_table
#[1]
create table bench00.spark_commit_jk 
as 
(select *, REGEXP_EXTRACT(message, "(SPARK-\\d{1,10})") as jkey 
	from bench00.spark_commit_records)






#cassandra_jira_keys
 create table bench00.cassandra_jira_sha 
 	as ( select j.*,s.sha  from 
         `bench00.cassandra_all_issue_records` j 
          LEFT OUTER JOIN 
          `bench00.hbase_sha_jira_keys` s # <== BUG! hbase???
           on j.key = s.key )


#cassandraX
create table bench00.cassandraX as 
(select string_field_0 as sha, 
	string_field_1 as key, 
	string_field_2 as module, 
	string_field_3 as filename 
from bench00.cassandra_sha_jira_key_module_file)

#cassandraY
create table bench00.cassandraY as 
(select string_field_0 as sha, string_field_1 as key 
	from `bench00.cassandra_sha_jira_keys`)

#commits_FOJ_issue_lifecycle_facts
create table bench5.commits_plus_FOJ_issue_lifecycle_facts as (
select * except (project) from bench5.all_commits_plus 
FULL OUTER JOIN 
`bench5.issue_lifecycle_facts` 
ON (yr=commit_yr and mo=commit_mon and day=commit_day)
)

#commits_issues_by_project
select project, count(distinct sha) as commits, count(distinct key) 
as issues from bench00.fiveplus group by project


#create_table_spark_cid_jkey
create table bench00.spark_cid_jkey 
as (select cid, jkey from `bench00.spark_commit_jk`)
#select count(*) from `bench00.spark_commit_records`  
#select count(*) from `bench00.spark_commit_jk`   

#create_table_spark_jkey_sev
create table bench00.spark_jkey_sev as (select key as jkey, severity as sev from `bench00.spark_issues_with_sev`)
#select count(*) from bench00.spark_issues_with_sev
#select count(*) from bench00.spark_jkey_sev

#FB
update bench5.five_core_facts set FB =
(case 
when issuetype='Bug' then 'Bug' 
else 'Feature' end) where 1=1

#file_changes
select filename, count(sha) as changes from `bench00.spark_issue_facts`  
group by filename order by changes desc limit 100

#issue_lifecycle_facts

create table bench5.issue_lifecycle_facts as
(select 
project,
extract(YEAR from created) as yr,
extract(QUARTER from created) as qtr,
extract(MONTH from created) as mo, 
extract(DAY from cr  eated) as day,
'created' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where created is not null)

UNION ALL

(select 
project,
extract(YEAR from updated) as yr,
extract(QUARTER from updated) as qtr,
extract(MONTH from updated) as mo, 
extract(DAY from updated) as day,
'updated' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where updated is not null)

UNION ALL

(select 
project,
extract(YEAR from resolved) as yr,
extract(QUARTER from resolved) as qtr,
extract(MONTH from resolved) as mo, 
extract(DAY from resolved) as day,
'resolved' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where resolved is not null)

#outer_join_scratch
#create table bench00.spark_outer_join_keys as (select * from `bench00.spark_cid_jkey` full outer join `bench00.spark_jkey_sev` using(jkey)) 
#where (jkey is null and cid is not null)
#select count(*) from `bench00.spark_cid_jkey` 
#select count(*) from `bench00.spark_jkey_sev` 
#select count(*) from `bench00.spark_jkey_sev` where jkey is not null
#select count(*) from `bench00.spark_outer_join_keys` 
#create table bench00.spark_commit_LOJ as (select * from `bench00.spark_outer_join_keys` a left outer join bench00.spark_commit_records b using (cid))
#create table bench00.spark_issue_commit_LOJ as (select * from `bench00.spark_commit_LOJ` c left outer join bench00.spark_all_issue_records d on (c.jkey=d.key))
#select count(*) from `bench00.spark_issue_commit_LOJ` 
#select count(*) from `bench00.spark_issues_with_sev`
#select count(*) from bench00.spark_jkey_sev 
#select count(*) from bench00.spark_issue_commit_LOJ where (jkey is not null and cid is not null)
select author_name, count(distinct cid) as commits from `bench00.spark_issue_commit_LOJ` group by author_name order by commits desc

#qdel_all_five_core_facts
#[2]
update `bench5.all_five_core_facts`  
set qdel = IF((status='Closed' or status='Resolved'), abs(qpts), 
       IF((issuetype='Bug' and status = 'Open'), qpts, 0) ) where 1=1


#qpts
#[3] BUG? is there a five_core_facts table or is this a bug
update bench5.five_core_facts set qpts = severity * (case 
when issuetype='Bug' then -1 
else 1 end) where 1=1


#qpts_all_five_core_facts
update bench5.all_five_core_facts set qpts = severity * (case 
when issuetype='Bug' then -1 
else 1 end) where 1=1

#quality_ratio_spark
select 
	(select count(*) from `bench5.issue_lifecycle_facts` 
	where isOpen=False and project='Spark')
	/ # division sign
	(select count(*) from `bench5.issue_lifecycle_facts` 
	where isOpen=True and issueType='Bug'and project='Spark') as QR



select author_name, count(distinct cid) as commits from `bench00.spark_issue_commit_LOJ` group by author_name order by commits desc

#qdel_all_five_core_facts
#[2]
update `bench5.all_five_core_facts`  
set qdel = IF((status='Closed' or status='Resolved'), abs(qpts), 
       IF((issuetype='Bug' and status = 'Open'), qpts, 0) ) where 1=1


#qpts
#[3] BUG? is there a five_core_facts table or is this a bug
update bench5.five_core_facts set qpts = severity * (case 
when issuetype='Bug' then -1 
else 1 end) where 1=1


#qpts_all_five_core_facts
update bench5.all_five_core_facts set qpts = severity * (case 
when issuetype='Bug' then -1 
else 1 end) where 1=1

#quality_ratio_spark
select 
	(select count(*) from `bench5.issue_lifecycle_facts` 
	where isOpen=False and project='Spark')
	/ # division sign
	(select count(*) from `bench5.issue_lifecycle_facts` 
	where isOpen=True and issueType='Bug'and project='Spark') as QR

#single_core_facts_query

create table bench5.spark_core_facts 
	as 
	( 	select *, REGEXP_EXTRACT(message, "(SPARK-\\d{1,10})") as jkey 
		from bench00.spark_commit_records a
		full outer join 
		bench00.spark_all_issue_records b
		on 
		(REGEXP_EXTRACT(a.message, "(SPARK-\\d{1,10})")=b.key)
	)

#spark_bug_dates

select key, jira_id, status, components, issuetype, created, 
updated, resolved, priority, severity 
from 
bench00.spark_all_issue_records
where issuetype = 'Bug'

#spark_commit_files

create table bench00.spark_commit_files 
as (select c.*,s.string_field_1 as filename  
	from 
	`bench00.spark_commit_records` c 
	 JOIN 
	 `bench00.spark_sha_files` s
	  on 
	 c.cid = s.string_field_0 
   )

#spark_contrib_commits
select project, count(distinct sha) as commits, 
count(distinct key) as issues 
from bench00.fiveplus 
group by project

#spark_issue_facts
create table bench00.spark_issue_facts 
	as 
(select j.*,c.*  from 
  `bench00.spark_jira_sha` j
  LEFT OUTER JOIN
  `bench00.spark_commit_module_file` c 
      on j.sha = c.cid )

#spark_issue_facts_x
create table bench00.spark_issues_facts_x 
	as 
	(select i.*,j.module from 
  `bench00.spark_issue_facts` i 
  JOIN `bench00.spark_sha_jira_key_module_file` s
      on i.filename = s.filename)

#spark_issues_count_medium
select status, count(status) as cnt from 
`bench00.spark_all_issue_records` 
where severity = 3 
group by status order by cnt desc

#spark_issues_group_count
select issuetype, status, severity, count(issuetype) as cnt 
from `bench00.spark_all_issue_records` 
where severity=3 and 
(status='Open' or status='Closed' or (status='Resolved' and resolution='Fixed'))
group by issuetype, status, severity order by issuetype, status, severity desc, cnt desc


#spark_issues_type_count
select issuetype, count(issuetype) as cnt from `bench00.spark_all_issue_records` group by issuetype
order by cnt desc

#spark_jira_sha

create table bench00.spark_jira_sha as ( select j.*,s.sha  from 
                                          `bench00.spark_all_issue_records` j 
                                          LEFT OUTER JOIN `bench00.spark_sha_jira_keys` s
                                              on j.key = s.key )
#spark_nonbug_dates
select key, jira_id, status, components, issuetype, created, updated, resolved, priority, severity from bench00.spark_all_issue_records
where issuetype != 'Bug'

#sumsev_yr_day_mo
select yr, qtr ,mo, day, project, issuetype, status ,sum(severity)as sumsev from `bench5.issue_lifecycle_facts` where yr > 2000 
group by yr,qtr,mo,day,project,issuetype,status order by yr desc, qtr, mo, day

#temp_severity_counts
#select severity, count(severity) as num, sum(severity) as tot from `bench5.five_core_facts` where status = 'Reopened' group by severity order by tot desc
select severity, count(severity) as num, sum(severity) as tot from `bench5.five_core_facts` group by severity order by tot desc
#select count(*) from bench5.all_commit_records

#update_week_in_datedim
update bench5.datedim set week = EXTRACT(WEEK FROM day) where 1=1

#UpdateIsBugIsOpen
update `bench5.issue_lifecycle_facts` 
	set 
	isBug = IF(issuetype='Bug', True, False), 
	isOpen = IF(status= 'Open', True, False) where 1=1

#updateValue
update `bench5.issue_lifecycle_facts` 
	set value = IF(isBug=true, -1*severity, severity) where 1=1

#wordone_qdel
#[4] Probably buggy or wrong
# wrong #update `bench5.five_core_facts` set workdone = IF((status != 'Open'), abs(qpts), 0) where 1=1
# wrong ##update `bench5.five_core_facts` set qdel = IF((status != 'Open'), abs(qpts), -1*abs(qpts)) where 1=1
update `bench5.five_core_facts` set qdel = IF((issuetype != 'Bug' and status='Open'), 0, qdel) where 1=1

#workdone_all_five_core_facts
update bench5.all_five_core_facts 
	set workdone = IF((status='Closed' or status = 'Resolved'), abs(qpts), 0) 
	here 1=1

#yr_mo_day_etc_sev
select yr, qtr ,mo, day, project, issuetype, status, severity, action, key 
from `bench5.issue_lifecycle_facts` where yr > 2000 
order by 
yr desc, qtr, mo, day, action, project, issuetype, status, severity desc





-- ======================================================================================================================================================
-- ======================================================================================================================================================

#cassandra_core_facts
create table bench5.cassandra_core_facts as (
select *, REGEXP_EXTRACT(message, "(CASSANDRA-\\d{1,10})") as jkey from bench00.cassandra_commit_records a
full outer join 
bench00.cassandra_all_issue_records b
on 
(REGEXP_EXTRACT(a.message, "(CASSANDRA-\\d{1,10})")=b.key)
)

#cassandra_jira_keys
 create table bench00.cassandra_jira_sha 
 	as ( select j.*,s.sha  from 
         `bench00.cassandra_all_issue_records` j 
          LEFT OUTER JOIN 
          `bench00.hbase_sha_jira_keys` s
           on j.key = s.key )

#create_commit_jk_table
#[1]
create table bench00.spark_commit_jk 
as 
(select *, REGEXP_EXTRACT(message, "(SPARK-\\d{1,10})") as jkey 
	from bench00.spark_commit_records)


#cassandraX
create table bench00.cassandraX as 
(select string_field_0 as sha, 
	string_field_1 as key, 
	string_field_2 as module, 
	string_field_3 as filename 
from bench00.cassandra_sha_jira_key_module_file)

#cassandraY
create table bench00.cassandraY as 
(select string_field_0 as sha, string_field_1 as key 
	from `bench00.cassandra_sha_jira_keys`)


#all_commit_records
create table bench5.all_commit_records as (
(select 'Spark' as project, * from bench5.spark_commit_records)
UNION ALL
(select 'Cassandra' as project, * from bench5.cassandra_commit_records)
UNION ALL
(select 'Hive' as project, * from bench5.hive_commit_records)
UNION ALL
(select 'HBase' as project, * from bench5.hbase_commit_records)
UNION ALL
(select 'Kafka' as project, * from bench5.kafka_commit_records)
)

#all_commits_plus
create table bench5.all_commits_plus as  
(
  select *,
  EXTRACT( YEAR FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_yr,
  EXTRACT( QUARTER FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_qtr,
  EXTRACT( MONTH FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_mon,
  EXTRACT( WEEK FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_wk,
  EXTRACT( DAY FROM (TIMESTAMP_SECONDS(time+time_offset )) ) as commit_day
from `bench5.all_commit_records`
)



#all_five_core_facts_union
create table bench5.all_five_core_facts as (
select * from bench5.spark_core_facts
UNION ALL
select * from bench5.hive_core_facts
UNION ALL
select * from bench5.hbase_core_facts
UNION ALL
select * from bench5.cassandra_core_facts
UNION ALL
select * from bench5.kafka_core_facts
)

#cassandra_commit_test 
select project, count(distinct sha) as commits, 
count(distinct key) as issues 
from bench00.cassandra_issue_facts 
group by project


#cassandra_SJK_from_cassandraY
 create table `bench00.cassandra_sha_jira_keys` 
 	as (select * from `bench00.cassandraY`)

#cassandraSJKMF_from_cassandraX
create table bench00.cassandra_sha_jira_key_module_file 
	as (select * from `bench00.cassandraX`)


#create_all_issue_records

create table bench5.all_issue_records as (
select * from bench5.cassandra_all_issue_records
union all
select * from bench5.hbase_all_issue_records
union all
select * from bench5.hive_all_issue_records
union all
select * from bench5.kafka_all_issue_records
union all
select * from bench5.spark_all_issue_records
)


#create_datedim_2010_18

create table bench5.datedim as (SELECT day
FROM UNNEST(
    GENERATE_DATE_ARRAY(DATE('2010-01-01'), DATE('2018-12-31'), INTERVAL 1 DAY)
) AS day)

#five_core_union_all
create table bench5.five_core_facts as (
select * from bench5.spark_core_facts
UNION ALL
select * from bench5.hive_core_facts
UNION ALL
select * from bench5.hbase_core_facts
UNION ALL
select * from bench5.cassandra_core_facts
UNION ALL
select * from bench5.kafka_core_facts
)

#hbase_commit_module_file
create table bench00.hbase_commit_module_file 
	as 
(select c.*,s.module,s.filename  
	from 
	`bench00.hbase_commit_records` c 
	JOIN `bench00.hbase_sha_jira_key_module_file` s
	  on c.cid = s.sha
)

#hbase_core_facts
create table bench5.hbase_core_facts as (
select *, REGEXP_EXTRACT(message, "(HBASE-\\d{1,10})") as jkey from bench5.hbase_commit_records a
full outer join 
bench5.hbase_all_issue_records b
on 
(REGEXP_EXTRACT(a.message, "(HBASE-\\d{1,10})")=b.key)

)

#hbase_issue_facts
create table bench00.hbase_issue_facts as 
	(select j.*,c.*  from                                       
              `bench00.hbase_jira_sha` j
               LEFT OUTER JOIN
               `bench00.hbase_commit_module_file` c 
     on j.sha = c.cid )


#hbase_jira_sha
create table bench00.hbase_jira_sha 
	as 
	( select j.*,s.sha  from 
      `bench00.hbase_all_issue_records` j 
      LEFT OUTER JOIN `bench00.hbase_sha_jira_keys` s
          on j.key = s.key )

#hive_commit_module_file
create table bench00.hive_commit_module_file 
	as (select c.*,s.module,s.filename  from 
        `bench00.hive_commit_records` c 
         JOIN `bench00.hive_sha_jira_key_module_file` s
         on c.cid = s.sha
        )

#hive_core_facts
create table bench5.hive_core_facts as (
	select *, REGEXP_EXTRACT(message, "(HIVE-\\d{1,10})") as jkey 
		from bench00.hive_commit_records a
	full outer 
	join 
	bench00.hive_all_issue_records b
	on 
	(REGEXP_EXTRACT(a.message, "(HIVE-\\d{1,10})")=b.key)
)

#hive_issue_facts
create table bench00.hive_issue_facts as 
	(select j.*,c.*  from 
          `bench00.hive_jira_sha` j
          LEFT OUTER JOIN
          `bench00.hive_commit_module_file` c 
      on j.sha = c.cid )

#hive_jira_sha
create table bench00.hive_jira_sha as 
	(select j.*,s.sha  from 
          `bench00.hive_all_issue_records` j 
          LEFT OUTER JOIN `bench00.hive_sha_jira_keys` s
          on j.key = s.key )

#issue_lifecycle_facts

create table bench5.issue_lifecycle_facts as
(select 
project,
extract(YEAR from created) as yr,
extract(QUARTER from created) as qtr,
extract(MONTH from created) as mo, 
extract(DAY from created) as day,
'created' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where created is not null)

UNION ALL

(select 
project,
extract(YEAR from updated) as yr,
extract(QUARTER from updated) as qtr,
extract(MONTH from updated) as mo, 
extract(DAY from updated) as day,
'updated' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where updated is not null)

UNION ALL

(select 
project,
extract(YEAR from resolved) as yr,
extract(QUARTER from resolved) as qtr,
extract(MONTH from resolved) as mo, 
extract(DAY from resolved) as day,
'resolved' as action, issuetype, status, key, severity
from `bench5.core_facts_five` where resolved is not null)


#kafka_core_facts
create table bench5.kafka_core_facts as (
	select *, REGEXP_EXTRACT(message, "(KAFKA-\\d{1,10})") as jkey 
	from bench00.kafka_commit_records a

	full outer join 

	bench00.kafka_all_issue_records b
	
	on 
	
	(REGEXP_EXTRACT(a.message, "(KAFKA-\\d{1,10})")=b.key)

)


#kafka_sha_jira_key_module_file
create table bench00.kafka_commit_module_file as 
	(select c.*,s.module,s.filename  from 
      `bench00.kafka_commit_records` c 
      JOIN `bench00.kafka_sha_jira_key_module_file` s
          on c.cid = s.sha
     )

#kafka_sha_jira_keys

create table bench00.kafka_jira_sha as 
	( select j.*,s.sha  from 
      `bench00.kafka_all_issue_records` j 
      LEFT OUTER JOIN `bench00.kafka_sha_jira_keys` s
          on j.key = s.key )

#multirecord_tasks
#select key, count(key) cnt from `bench5.core_facts_five` group by key order by cnt desc
#select project, key, count(key) as cnt from `bench5.core_facts_five` group by project,key order by cnt desc
#select * from `bench5.issue_lifecycle_facts` where key = 'KAFKA-1650'
#select * from `bench5.issue_lifecycle_facts` where key = 'HIVE-9418'
select * from `bench5.issue_lifecycle_facts` where key = 'HIVE-14990'

#outer_join_scratch
#create table bench00.spark_outer_join_keys as (select * from `bench00.spark_cid_jkey` full outer join `bench00.spark_jkey_sev` using(jkey)) 
#where (jkey is null and cid is not null)
#select count(*) from `bench00.spark_cid_jkey` 
#select count(*) from `bench00.spark_jkey_sev` 
#select count(*) from `bench00.spark_jkey_sev` where jkey is not null
#select count(*) from `bench00.spark_outer_join_keys` 
#create table bench00.spark_commit_LOJ as (select * from `bench00.spark_outer_join_keys` a left outer join bench00.spark_commit_records b using (cid))
#create table bench00.spark_issue_commit_LOJ as (select * from `bench00.spark_commit_LOJ` c left outer join bench00.spark_all_issue_records d on (c.jkey=d.key))
#select count(*) from `bench00.spark_issue_commit_LOJ` 
#select count(*) from `bench00.spark_issues_with_sev`
#select count(*) from bench00.spark_jkey_sev 
#select count(*) from bench00.spark_issue_commit_LOJ where (jkey is not null and cid is not null)
select author_name, count(distinct cid) as commits from `bench00.spark_issue_commit_LOJ` group by author_name order by commits desc

#qdel_all_five_core_facts
#[2]
update `bench5.all_five_core_facts`  
set qdel = IF((status='Closed' or status='Resolved'), abs(qpts), 
       IF((issuetype='Bug' and status = 'Open'), qpts, 0) ) where 1=1


#qpts
#[3] BUG? is there a five_core_facts table or is this a bug
update bench5.five_core_facts set qpts = severity * (case 
when issuetype='Bug' then -1 
else 1 end) where 1=1


#qpts_all_five_core_facts
update bench5.all_five_core_facts set qpts = severity * (case 
when issuetype='Bug' then -1 
else 1 end) where 1=1

#quality_ratio_spark
select 
	(select count(*) from `bench5.issue_lifecycle_facts` 
	where isOpen=False and project='Spark')
	/ # division sign
	(select count(*) from `bench5.issue_lifecycle_facts` 
	where isOpen=True and issueType='Bug'and project='Spark') as QR

#single_core_facts_query

create table bench5.spark_core_facts 
	as 
	( 	select *, REGEXP_EXTRACT(message, "(SPARK-\\d{1,10})") as jkey 
		from bench00.spark_commit_records a
		full outer join 
		bench00.spark_all_issue_records b
		on 
		(REGEXP_EXTRACT(a.message, "(SPARK-\\d{1,10})")=b.key)
	)

#spark_bug_dates

select key, jira_id, status, components, issuetype, created, 
updated, resolved, priority, severity 
from 
bench00.spark_all_issue_records
where issuetype = 'Bug'

#spark_commit_files

create table bench00.spark_commit_files 
as (select c.*,s.string_field_1 as filename  
	from 
	`bench00.spark_commit_records` c 
	 JOIN 
	 `bench00.spark_sha_files` s
	  on 
	 c.cid = s.string_field_0 
   )

#spark_contrib_commits
select project, count(distinct sha) as commits, 
count(distinct key) as issues 
from bench00.fiveplus 
group by project

#spark_issue_facts
create table bench00.spark_issue_facts 
	as 
(select j.*,c.*  from 
  `bench00.spark_jira_sha` j
  LEFT OUTER JOIN
  `bench00.spark_commit_module_file` c 
      on j.sha = c.cid )

#spark_issue_facts_x
create table bench00.spark_issues_facts_x 
	as 
	(select i.*,j.module from 
  `bench00.spark_issue_facts` i 
  JOIN `bench00.spark_sha_jira_key_module_file` s
      on i.filename = s.filename)

#spark_issues_count_medium
select status, count(status) as cnt from 
`bench00.spark_all_issue_records` 
where severity = 3 
group by status order by cnt desc

#spark_issues_group_count
select issuetype, status, severity, count(issuetype) as cnt 
from `bench00.spark_all_issue_records` 
where severity=3 and 
(status='Open' or status='Closed' or (status='Resolved' and resolution='Fixed'))
group by issuetype, status, severity order by issuetype, status, severity desc, cnt desc


#spark_issues_type_count
select issuetype, count(issuetype) as cnt from `bench00.spark_all_issue_records` group by issuetype
order by cnt desc

#spark_jira_sha

create table bench00.spark_jira_sha as ( select j.*,s.sha  from 
                                          `bench00.spark_all_issue_records` j 
                                          LEFT OUTER JOIN `bench00.spark_sha_jira_keys` s
                                              on j.key = s.key )
#spark_nonbug_dates
select key, jira_id, status, components, issuetype, created, updated, resolved, priority, severity from bench00.spark_all_issue_records
where issuetype != 'Bug'

#sumsev_yr_day_mo
select yr, qtr ,mo, day, project, issuetype, status ,sum(severity)as sumsev from `bench5.issue_lifecycle_facts` where yr > 2000 
group by yr,qtr,mo,day,project,issuetype,status order by yr desc, qtr, mo, day

#temp_severity_counts
#select severity, count(severity) as num, sum(severity) as tot from `bench5.five_core_facts` where status = 'Reopened' group by severity order by tot desc
select severity, count(severity) as num, sum(severity) as tot from `bench5.five_core_facts` group by severity order by tot desc
#select count(*) from bench5.all_commit_records

#update_week_in_datedim
update bench5.datedim set week = EXTRACT(WEEK FROM day) where 1=1

#UpdateIsBugIsOpen
update `bench5.issue_lifecycle_facts` 
	set 
	isBug = IF(issuetype='Bug', True, False), 
	isOpen = IF(status= 'Open', True, False) where 1=1

#updateValue
update `bench5.issue_lifecycle_facts` 
	set value = IF(isBug=true, -1*severity, severity) where 1=1

#wordone_qdel
#[4] Probably buggy or wrong
# wrong #update `bench5.five_core_facts` set workdone = IF((status != 'Open'), abs(qpts), 0) where 1=1
# wrong ##update `bench5.five_core_facts` set qdel = IF((status != 'Open'), abs(qpts), -1*abs(qpts)) where 1=1
update `bench5.five_core_facts` set qdel = IF((issuetype != 'Bug' and status='Open'), 0, qdel) where 1=1

#workdone_all_five_core_facts
update bench5.all_five_core_facts 
	set workdone = IF((status='Closed' or status = 'Resolved'), abs(qpts), 0) 
	here 1=1

#yr_mo_day_etc_sev
select yr, qtr ,mo, day, project, issuetype, status, severity, action, key 
from `bench5.issue_lifecycle_facts` where yr > 2000 
order by 
yr desc, qtr, mo, day, action, project, issuetype, status, severity desc

#############
#[1] .spark_commit_jk
# select author_name, count(distinct cid) as commits from `bench00.spark_commit_records` group by author_name order by commits desc
#select author_name, count(sha) as commits from `bench00.spark_issue_facts` group by author_name order by commits desc
#select count(distinct sha) from `bench00.hbase_sha_jira_keys`  
#select count(distinct sha) from bench00.hbase_issue_facts

#select sha from `bench00.hbase_sha_jira_keys`  
#where sha  in 
#(select distinct sha from bench00.hbase_issue_facts)
#select sha, count(sha) as occurs from `bench00.hbase_issue_facts` group by sha order by occurs desc

#select sha, count(sha) as occurs from `bench00.spark_sha_jira_keys` group by sha order by occurs desc
#select cid, count(cid) as occurs from `bench00.spark_commit_records` group by cid order by occurs desc

#select REGEXP_EXTRACT(message, "^\\[(SPARK-\\d{1,10})\\]") as jkey from bench00.spark_commit_records
create table bench00.spark_commit_jk as (select *, REGEXP_EXTRACT(message, "(SPARK-\\d{1,10})") as jkey from bench00.spark_commit_records)
#select *, REGEXP_EXTRACT(message, "(SPARK-\\d{1,10})") as jkey from bench00.spark_commit_records limit 1000

#[2] there may be a bug in this query in the second IF related to 0




