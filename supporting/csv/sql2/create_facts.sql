-- pipeline 2
-- spark_all_issue_records
-- spark_commit_records
-- spark_jira_key_sev
-- spark_sha_files
-- spark_sha_jira_key_module_file
-- spark_sha_jira_keys

-- create all fact tables

-- prep

create table`spark_datedim` as (SELECT day
FROM UNNEST(
    GENERATE_DATE_ARRAY(DATE('2010-01-01'), DATE('2019-12-31'), INTERVAL 1 DAY)
) AS day)


## ISSUE_LIFECYCLE_FACTS

# issue_lifecycle_facts
# needs: all_issue_records

create table spark_issue_lifecycle_facts as
(select
project,
extract(YEAR from created) as yr,
extract(QUARTER from created) as qtr,
extract(MONTH from created) as mo,
extract(DAY from created) as day,
'created' as action, issuetype, status, key, severity
from spark_all_issue_records where created is not null)

UNION ALL

(select
project,
extract(YEAR from updated) as yr,
extract(QUARTER from updated) as qtr,
extract(MONTH from updated) as mo,
extract(DAY from updated) as day,
'updated' as action, issuetype, status, key, severity
from spark_all_issue_records where updated is not null)

UNION ALL

(select
project,
extract(YEAR from resolved) as yr,
extract(QUARTER from resolved) as qtr,
extract(MONTH from resolved) as mo,
extract(DAY from resolved) as day,
'resolved' as action, issuetype, status, key, severity
from spark_all_issue_records where resolved is not null);

ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN vpts integer;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN vdel integer;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN workdone integer;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN bugorfeature varchar(10);

ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN isbug boolean;
ALTER TABLE spark_issue_lifecycle_facts ADD COLUMN isopen boolean;

#UpdateIsBugIsOpen
update `spark_issue_lifecycle_facts` 
	set 
	isbug = IF(issuetype='Bug', 1, 0), 
	isopen = IF(status= 'Open', 1, 0) 

#value_points
update `spark_issue_lifecycle_facts` 
	set vpts = severity * IF(isbug, -1, 1) 

#work_done
update `spark_issue_lifecycle_facts`
	set workdone = IF((status='Closed' or status = 'Resolved'), abs(vpts), 0) 
	
#value_delivered
update `spark_issue_lifecycle_facts` 
set vdel = workdone + IF(isbug AND isopen, vpts, 0)

#FB

#FB
update `spark_issue_lifecycle_facts` 
set bugorfeature = (case when issuetype='Bug' then 'Bug' else 'Feature' end) 


-- #qpts
-- update `spark_issue_lifecycle_facts`
-- set qpts = severity * IF(isBug, -1, 1) 

#quality_ratio_spark
select 
	(select count(*) from `spark_issue_lifecycle_facts` 
	where isOpen=False and project='Spark') # closed items
	/ # division sign
	(select count(*) from `spark_issue_lifecycle_facts`  # open items
	where isOpen=True and issueType='Bug'and project='Spark') as quality_ratio_spark




-- ====




