#!/usr/bin/env bash

for file in SPARK*
do
    # ${file/#F0000/F000} means replace the pattern that starts at beginning of string
    mv "$file" "${file/#SPARK/spark}"
done