# remove NOT NULL clause from auto generated DDL
for f in spark*sql; do
  mv $f $f.tmp
done
for f in spark*sql.tmp; do
  sed s/NOT\ NULL//g $f > ${f%.tmp}
done
rm *.tmp