import pickle
import copy
import pathlib
import dash
import math
from datetime import datetime as dt
import pandas as pd
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash_core_components as dcc
import dash_html_components as html


DATA_PATH = '/Users/nitin/develop/git/nupg/clientalpha/NuDash/data'

# Load data
df_issue = pd.read_csv(DATA_PATH + "/spark_all_issue_records.csv", low_memory=False)
df_commit = pd.read_csv(DATA_PATH + "/spark_commit_records.csv", low_memory=False)
df_core = pd.read_csv(DATA_PATH + "/spark_core_facts.csv", low_memory=False)
df_lifecycle = pd.read_csv(DATA_PATH + "/spark_issue_lifecycle_facts.csv", low_memory=False)

# convert unix time for human consumption
df_commit['dates'] = (df_commit['time'] + df_commit['time_offset']).apply(dt.fromtimestamp)
df_commit['yr'] = df_commit['dates'].apply(lambda x: x.year)
df_commit['mon'] = df_commit['dates'].apply(lambda x: x.month)
df_commit['day'] = df_commit['dates'].apply(lambda x: x.day)
df_commit['DOW'] = df_commit['dates'].apply(lambda x: x.day_name())
df_commit['hr'] =df_commit['dates'].apply(lambda x: x.hour)


dataf = {
    "commits": df_commit,
    "issues": df_issue,
    "core": df_core,
    "lifecycle": df_lifecycle
}


# Create controls from spark dataframes

nu_controls = {
    PRIORITIES = df_issue['priority'].value_counts(),
    PRIORITY = PRIORITIES.index.to_list(),
    
    ISSUESTATUSES = df_issue['status'].value_counts(),
    ISSUESTATUSES_5 = ISSUESTATUSES[0:5],
    ISSUESTATUS = ISSUESTATUSES.index.to_list(),
    
    ISSUETYPES = df_issue['issuetype'].value_counts(),
    ISSUETYPES_5 = ISSUETYPES[0:5],
    ISSUETYPE = ISSUETYPES.index.to_list(),
    
    COMPONENTS = df_issue['components'].value_counts(),
    COMPONENTS_5 = COMPONENTS[0:5],
    COMPONENTS_10 = COMPONENTS[0:10],
    COMPONENTS_20 = COMPONENTS[0:20],
    COMPONENT = COMPONENTS.index.to_list(),
# Lifecycle
    YEARS = list(df_commit['yr'].unique()),
    YEARS.sort(),
    YEARS_LAST5 = YEARS[-5:],
    YEARS_LAST3 = YEARS[-3:],
    LAST_YEAR = YEARS[-1]
}

def year_start_end(year):
    year_start = "str(year)"




def isClosed(issuekey):
    return


def isOpen(issuekey):
    return


def isBug(issuetype):
    return

# takes the output of some_data_frame.value_counts()
# returns a list which is a subset of the index or all of it
def display_list(val_count, limit=None):
    ret = val_count.index.to_list()
    if limit:
        ret = ret[0:limit]
    return ret

def histogram(val_count):
    #make a histogram with item, valcount
    return


filter_data_frame
    