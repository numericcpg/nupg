# Import required libraries
import pickle
import copy
import pathlib
import dash
import math
#import datetime as dt
from datetime import datetime as dt
import pandas as pd
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px

from sqlalchemy import create_engine

pg_url = "postgresql://numericc:numericc@localhost:5432/nupg"
engine = create_engine(pg_url)
conn = engine.connect()
qry = "select l.* , i.components " \
      "from spark_issue_lifecycle_facts l, spark_all_issue_records i " \
      "where l.key = i.key"
df_ilfcomp = pd.read_sql(qry,conn)


# Multi-dropdown options
from controls import COUNTIES, WELL_STATUSES, WELL_TYPES, WELL_COLORS


app = dash.Dash(
    __name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}],
    external_stylesheets=["https://codepen.io/chriddyp/pen/bWLwgP.css"]
)
server = app.server


# get relative data folder
PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("data").resolve()

# use this for py console
# cur_dir = !pwd
# CUR_PATH=pathlib.Path(cur_dir[0])
# DATA_PATH=CUR_PATH.joinpath("NuDash").joinpath("data") <-

points = pickle.load(open(DATA_PATH.joinpath("points.pkl"), "rb"))

gapminder = px.data.gapminder()

dimensions = []

# Load data
df_issue = pd.read_csv(DATA_PATH.joinpath("spark_all_issue_records.csv"), low_memory=False)
df_commit = pd.read_csv(DATA_PATH.joinpath("spark_commit_records.csv"), low_memory=False)
df_core = pd.read_csv(DATA_PATH.joinpath("spark_core_facts.csv"), low_memory=False)
df_lifecycle = pd.read_csv(DATA_PATH.joinpath("spark_issue_lifecycle_facts.csv"), low_memory=False)
df_ilfc = df_lifecycle.join(df_issue.set_index('key'), on='key', how='inner', lsuffix='ilf', rsuffix='i')
df_lifecycle['components'] = df_ilfc['components']
df_lifecycle["created"] = df_ilfc["created"]

# convert unix time for human consumption
df_commit['dates'] = (df_commit['time'] + df_commit['time_offset']).apply(dt.fromtimestamp)
df_commit['yr'] = df_commit['dates'].apply(lambda x: x.year)
df_commit['mon'] = df_commit['dates'].apply(lambda x: x.month)
df_commit['day'] = df_commit['dates'].apply(lambda x: x.day)
df_commit['DOW'] = df_commit['dates'].apply(lambda x: x.day_name())
df_commit['hr'] =df_commit['dates'].apply(lambda x: x.hour)


PRIORITYVC = df_issue['priority'].value_counts()
PRIORITIES = PRIORITYVC.index.to_list()

ISSUESTATUSVC = df_issue['status'].value_counts()
ISSUESTATUSES = ISSUESTATUSVC.index.to_list()

ISSUETYPEVC = df_issue['issuetype'].value_counts()
ISSUETYPEVC_5 = ISSUETYPEVC[0:5]
ISSUETYPES = ISSUETYPEVC.index.to_list()

COMPONENTSVC = df_issue['components'].value_counts()
COMPONENTSVC_5 = COMPONENTSVC[0:5]
COMPONENTSVC_10 = COMPONENTSVC[0:10]
COMPONENTSVC_20 = COMPONENTSVC[0:20]
COMPONENTS = COMPONENTSVC.index.to_list()

# Lifecycle
YEARS = list(df_commit['yr'].unique())
YEARS.sort()
YEARS_LAST5 = YEARS[-5:]
YEARS_LAST3 = YEARS[-3:]
LAST_YEAR = YEARS[-1]

issuetype_options = [
    {"label": str(issuetype), "value": str(issuetype)} for issuetype in ISSUETYPES
]

issuestatus_options =[
    {"label": issuestatus, "value": issuestatus} for issuestatus in ISSUESTATUSES
]

priority_options =[
    {"label": str(priority), "value": str(priority)} for priority in PRIORITIES

]

component_options =[
    {"label": str(component), "value": str(component)} for component in COMPONENTS
]


year_option=[
    {"label": str(yr), "value": str(yr)} for yr in YEARS
]



county_options = [
    {"label": str(COUNTIES[county]), "value": str(county)} for county in COUNTIES
]

well_status_options = [
    {"label": str(WELL_STATUSES[well_status]), "value": str(well_status)}
    for well_status in WELL_STATUSES
]

well_type_options = [
    {"label": str(WELL_TYPES[well_type]), "value": str(well_type)}
    for well_type in WELL_TYPES
]


"""
df = pd.read_csv(DATA_PATH.joinpath("wellspublic.csv"), low_memory=False)
df["Date_Well_Completed"] = pd.to_datetime(df["Date_Well_Completed"])
df = df[df["Date_Well_Completed"] > dt(1960, 1, 1)]

trim = df[["API_WellNo", "Well_Type", "Well_Name"]]
trim.index = trim["API_WellNo"]
dataset = trim.to_dict(orient="index")

# load fact tables

# Create global chart template
mapbox_access_token = "pk.eyJ1IjoiamFja2x1byIsImEiOiJjajNlcnh3MzEwMHZtMzNueGw3NWw5ZXF5In0.fk8k06T96Ml9CLGgKmk81w"
"""

layout = dict(
    autosize=True,
    automargin=True,
    margin=dict(l=30, r=30, b=20, t=40),
    hovermode="closest",
    plot_bgcolor="#F9F9F9",
    paper_bgcolor="#F9F9F9",
    legend=dict(font=dict(size=10), orientation="h"),
    title="Satellite Overview"
)

app.layout = html.Div(
    [
        dcc.Store(id="aggregate_data"),
        # empty Div to trigger javascript file for graph resizing
        html.Div(id="output-clientside"),
        
        # Rows
        html.Div([
            
            # Row 0 Totals
            # ======
            html.Div(
                [
                    html.Div(
                        [
                            html.Img(
                                #src=app.get_asset_url("dash-logo.png"),
                                src=app.get_asset_url("NumericcFullLogo.png"),
                                id="plotly-image",
                                style={
                                    "height": "60px",
                                    "width": "auto",
                                    "margin-bottom": "25px",
                                },
                            )
                        ],
                        className="one-third column",
                    ),
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H3(
                                        "Engineering Intelligence Dashboard",
                                        style={"margin-bottom": "0px"},
                                    ),
                                    html.H5(
                                        "Issue and Change View", style={"margin-top": "0px"}
                                    ),
                                ]
                            )
                        ],
                        className="one-half column",
                        id="title",
                    ),
                    html.Div(
                        [
                            html.A(
                                html.Button("Learn More", id="learn-more-button"),
                                # href="https://plot.ly/dash/pricing/",
                                href="http://numericc.com",
                            )
                        ],
                        className="one-third column",
                        id="button",
                    ),
                ],
                id="header",
                className="row flex-display",
                style={"margin-bottom": "25px"},
            ),
            # End Row 0 Div
            # ====
            
            # Row 1 Selection panel
            # =====
            html.Div(
                [
                    html.Div(
                        # left selector panel
                        [
                            html.P(
                                "Filter by creation date (or select range in histogram):",
                                className="control_label",
                            ),
                            dcc.RangeSlider(
                                id="year_slider",
                                min=2010,
                                max=2019,
                                value=[2010, 2019],
                                className="dcc_control",
                            ),
                            html.P("Filter by component:", className="control_label"),
                            dcc.Dropdown(
                                id="components",
                                options=component_options,
                                multi=True,
                                value=list(COMPONENTSVC_20.index),
                                className="dcc_control",
                            ),
                            
                            html.P("Filter by issue type:",
                                   className="control_label"),
                            dcc.Dropdown(
                                id="issuetypes",
                                options=issuetype_options,
                                multi=True,
                                value=list(ISSUETYPES),
                                className="dcc_control",
                            ),
                        ],
                        className="pretty_container four columns",
                        id="cross-filter-options",
                    ),
                    html.Div(
                        [
                            html.Div(
                                # summary totals
                                [
                                    html.Div(
                                        [html.H6(id="chg_text"), html.P("Changes")],
                                        id="changes",
                                        className="mini_container",
                                    ),
                                    html.Div(
                                        [html.H6(id="bugText"), html.P("Bugs")],
                                        id="bugs",
                                        className="mini_container",
                                    ),
                                    html.Div(
                                        [html.H6(id="featureText"), html.P("Features")],
                                        id="features",
                                        className="mini_container",
                                    ),
                                    html.Div(
                                        [html.H6(id="closedText"), html.P("Closed")],
                                        id="closed",
                                        className="mini_container",
                                    ),
                                ],
                                id="info-container",
                                className="row container-display",
                            ),
                            html.Div(
                                    [
                                        html.H1("Evolution Overview"),
                                        dcc.Graph(id="graph2", style={"width": "75%", "display": "inline-block"},
                                        figure=px.scatter(df_lifecycle, x="workdone", y="severity",
                                                          animation_frame="yr", animation_group="components",
                                                      size="mo", color="status", hover_name="key",
                                                      log_x=True, size_max=55, range_x=[1,20], range_y=[10,30])),
                                    ]
                            ),
                            
                            """html.Div(
                                # count graph histogram
                                [dcc.Graph(id="count_graph")],
                                id="countGraphContainer",
                                className="pretty_container",
                            ),"""
                        ],
                        id="right-column",
                        className="eight columns",
                    ),
                ],
                className="row flex-display",
            ),
            # End Row 1 Div
            # =====
        
            # Row 2
            # =====
            html.Div(
                [
                    html.Div(
                        [
                            # Issue histogram
                            html.H1("Issue slice and dice"),
                            dcc.Graph(id="graph", style={"width": "75%", "display": "inline-block"},
                            #figure=px.scatter(gapminder, x="gdpPercap", y="lifeExp", animation_frame="year", animation_group="country",
                                          #size="pop", color="continent", hover_name="country",
                                          #log_x=True, size_max=55, range_x=[100,100000], range_y=[25,90])),
                            figure = px.scatter(df_issue,
                                                x="severity", y="issuetype",
                                                color="status",
                                                color_discrete_map = {"Resolved":"blue", "Open":"red", "In Progress":"green","Closed":"orange","Reopened":"pink"},
                                                marginal_y="histogram", marginal_x="histogram")),
                        
                        ],
                        className="pretty_container seven columns",
                    ),
                    html.Div(
                        # dist_plot w rug, hist kde for single module metric
                        [dcc.Graph(id="individual_graph")],
                        className="pretty_container five columns",
                    ),
                ],
                className="row flex-display",
            ),
            # End Row 2 Div
            # Row 3
            # =====
            html.Div(
                [
                    html.Div(
                        [dcc.Graph(id="pie_graph")],
                        className="pretty_container seven columns",
                    ),
                    html.Div(
                        [dcc.Graph(id="aggregate_graph")],
                        className="pretty_container five columns",
                    ),
                ],
                className="row flex-display",
            ), # End Row 3 Div
            # =====
    
        ]) # End Rows div
    ],
    id = "mainContainer",
    style = {"display": "flex", "flex-direction": "column"},

) # top level div

# Helper functions
def human_format(num):

    magnitude = int(math.log(num, 1000))
    mantissa = str(int(num / (1000 ** magnitude)))
    return mantissa + ["", "K", "M", "G", "T", "P"][magnitude]


def old_filter_dataframe(df, well_statuses, well_types, year_slider):
    dff = df[
        df["Well_Status"].isin(well_statuses)
        & df["Well_Type"].isin(well_types)
        & (df["Date_Well_Completed"] > dt(year_slider[0], 1, 1))
        & (df["Date_Well_Completed"] < dt(year_slider[1], 1, 1))
    ]
    return dff


def filter_dataframe(df_in, components, issuetypes, year_slider):
    dff = None
    try:
        dff = df_in[
            df_in["components"].isin(components)
            & df_in["issuetype"].isin(issuetypes)
            & (pd.to_datetime(df_in["created"]) > dt(year_slider[0], 1, 1))
            & (pd.to_datetime(df_in["created"]) < dt(year_slider[1], 1, 1))
        ]
    except:
    #TODO Fill me!
        pass
    
    return dff



def produce_individual(component_name):
    try:
        
        bu =  df_lifecycle[df_lifecycle['components'] == component_name]['isbug'].value_counts(True)
        fe =  df_lifecycle[df_lifecycle['components'] == component_name]['isbug'].value_counts(False)
        cl =  df_lifecycle[df_lifecycle['components'] == component_name]['isopen'].value_counts(False)
        op =  df_lifecycle[df_lifecycle['components'] == component_name]['isopen'].value_counts(True)
        
    except:
        # print exception details to console
        return None, None, None, None, None

    index = YEARS

    changes = []
    bugs = []
    features = []
    closed =[]
    open =[]
    
    try:
        changes.append(chg)
    except:
        changes.append(0)
    try:
        bugs.append()
    except:
        bugs.append(0)
    try:
        features.append(points[component_name][year]["Water Produced, bbl"])
    except:
        features.append(0)
    try:
        closed.append(points[component_name][year]["Water Produced, bbl"])
    except:
        closed.append(0)
    try:
        open.append(points[component_name][year]["Water Produced, bbl"])
    except:
        open.append(0)
    
    return changes, bugs, features, closed # also add open when layout changed



def produce_aggregate(selected, year_slider):
    
    try:
        pass
    except:
        # print exception details to console
        return None, None, None, None
    
    index = list(range(max(year_slider[0], 2010), 2020))
    print(index)
    bugs = []
    features = []
    closed = []
    open = []
    
    for year in index:
        count_bugs = 0
        count_features = 0
        count_closed = 0
        count_open = 0
        
        for component_name in selected:
            try:
                count_bugs += df_lifecycle[(df_lifecycle['components'] == component_name) & (df_lifecycle['yr'] == year)]['isbug'].value_counts()[True]
            except:
                pass
            try:
                count_features += df_lifecycle[(df_lifecycle['components'] == component_name) & (df_lifecycle['yr'] == year)]['isbug'].value_counts()[False]
            except:
                pass
            try:
                count_closed += df_lifecycle[(df_lifecycle['components'] == component_name) & (df_lifecycle['yr'] == year)]['isopen'].value_counts()[False]
            except:
                pass
            try:
                count_open += df_lifecycle[(df_lifecycle['components'] == component_name) & (df_lifecycle['yr'] == year)]['isopen'].value_counts()[True]
            except:
                pass

        bugs.append(count_bugs)
        features.append(count_features)
        closed.append(count_closed)
        open.append(count_open)
    
    print(bugs)
    print(features)
    print(closed)

    return index, bugs, features, closed
    
    
# Selectors -> chg_text
@app.callback(
    Output("chg_text", "children"),
    [
        Input("components", "value"),
        Input("issuetypes", "value"),
        Input("year_slider", "value"),
    ],
)

def update_well_text(components, issuetypes, year_slider):
    dff = filter_dataframe(df_core, components, issuetypes, year_slider)
    chgs = dff['cid'].value_counts().index.size
    return chgs


@app.callback(
    Output("aggregate_data", "data"),
    [
        Input("components", "value"),
        Input("issuetypes", "value"),
        Input("year_slider", "value"),
    ],
)
def update_production_text(components, issuetypes, year_slider):

    #dff = filter_dataframe(df_lifecycle, components, issuetypes, year_slider)
    #selected = dff['components'].to_list()
    index, bugs, features, closed = produce_aggregate(components, year_slider)
    print(bugs)
    return [sum(bugs)//10, sum(features)//10, sum(closed)//10]


@app.callback(
    [
        Output("bugText", "children"),
        Output("featureText", "children"),
        Output("closedText", "children"),
    ],
    [Input("aggregate_data", "data")],
)
def update_text(data):
    return data[0],data[1],data[2]
#    return 10,20,30

"""
# Selectors, main graph -> pie graph
@app.callback(
    Output("pie_graph", "figure"),
    [
        Input("components", "value"),
        Input("issuetypes", "value"),
        Input("year_slider", "value"),
    ],
)
def make_pie_figure(components, issuetypes, year_slider):

    layout_pie = copy.deepcopy(layout)

    dff = filter_dataframe(df_issue, components, issuetypes, year_slider)

    selected = components
    index, gas, oil, water = produce_aggregate(selected, year_slider)

    aggregate = dff.groupby(['status']).count()

    data = [
        dict(
            type="pie",
            labels=["Gas", "Oil", "Water"],
            values=[sum(gas), sum(oil), sum(water)],
            name="Production Breakdown",
            text=[
                "Total Gas Produced (mcf)",
                "Total Oil Produced (bbl)",
                "Total Water Produced (bbl)",
            ],
            hoverinfo="text+value+percent",
            textinfo="label+percent+name",
            hole=0.5,
            marker=dict(colors=["#fac1b7", "#a9bb95", "#92d8d8"]),
            domain={"x": [0, 0.45], "y": [0.2, 0.8]},
        ),
        dict(
            type="pie",
            labels=[WELL_TYPES[i] for i in aggregate.index],
            values=aggregate["API_WellNo"],
            name="Well Type Breakdown",
            hoverinfo="label+text+value+percent",
            textinfo="label+percent+name",
            hole=0.5,
            marker=dict(colors=[WELL_COLORS[i] for i in aggregate.index]),
            domain={"x": [0.55, 1], "y": [0.2, 0.8]},
        ),
    ]
    layout_pie["title"] = "Production Summary: {} to {}".format(
        year_slider[0], year_slider[1]
    )
    layout_pie["font"] = dict(color="#777777")
    layout_pie["legend"] = dict(
        font=dict(color="#CCCCCC", size="10"), orientation="h", bgcolor="rgba(0,0,0,0)"
    )

    figure = dict(data=data, layout=layout_pie)
    return figure

"""

from sqlalchemy import create_engine
pgurl = "postgresql://numericc:numericc@localhost:5432/nupg"
engine = create_engine(pgurl)
con = engine.connect()
df = pd.read_sql('select count(*) from spark_commit_records', con)

def isClosed(issuekey):
    return


def isOpen(issuekey):
    return


def isBug(issuetype):
    return

# takes the output of some_data_frame.value_counts()
# returns a list which is a subset of the index or all of it
def display_list(val_count, limit=None):
    ret = val_count.index.to_list()
    if limit:
        ret = ret[0:limit]
    return ret

def histogram(val_count):
    #make a histogram with item, valcount
    return


# Main
if __name__ == "__main__":
    app.run_server(debug=True, port=10050)
